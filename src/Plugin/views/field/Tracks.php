<?php

namespace Drupal\rfn_views\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides Tracks field handler.
 *
 * @ViewsField("rfn_views_tracks")
 */
class Tracks extends FieldPluginBase {

  use StringTranslationTrait;

  /**
   * Query function - intentionally left blank.
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $node = $values->_entity;
    $value = t('Track count for this artist');
    $value .= ': ';

    // Get the album  nodes that have this artist id in its field_artists.
    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties(['type' => 'album', 'field_artists' => $node->id()]);

    $trackTotal = 0;
    foreach ($nodes as $nid => $_found) {

      $count = count($_found->field_media_items->getValue());
      $trackTotal += $count;
    }

    $value = $this->formatPlural($trackTotal, '1 track', '@count tracks');

    return $value;
  }

}
