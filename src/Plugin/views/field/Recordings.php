<?php

namespace Drupal\rfn_views\Plugin\views\field;

use Drupal\Core\Render\Markup;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides Recordings field handler.
 *
 * @ViewsField("rfn_views_recordings")
 *
 * Provides a views plugin that shows the number of recordings for an Artist.
 */
class Recordings extends FieldPluginBase {

  use StringTranslationTrait;

  /**
   * Query function - intentionally blank.
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = parent::render($values);
    $node = $values->_entity;

    // Get the album nodes that have this artist id in its field_artists.
    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties(['type' => 'album', 'field_artists' => $node->id()]);

    $count = count($nodes);
    if ($count === 0) {
      return $this->t(
            '<span style="color: red; font-weight: bold;">%artist_name has no recordings in the database and should be removed.</span>',
            ['%artist_name' => $node->getTitle()]
        );
    }
    else {

      $value = $this->formatPlural($count, '1 Recording', '@count Recordings');
      $value .= '<ul>';
      foreach ($nodes as $nid => $_found) {

        $link = $_found->toLink()->toRenderable();
        $link['#attributes']['title'] = t('Click to explore recording %title', ['%title' => $_found->getTitle()]);
        $value .= '<li>' . drupal_render($link) . '</li>';
      }
      $value .= '</ul>';

      return Markup::create($value);
    }

  }

}
