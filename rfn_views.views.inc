<?php

/**
 * @file
 * rfn_views.views.inc
 */

/**
 * Implements hook_views_data_alter().
 *
 * @var $data - an array of views data
 */
function rfn_views_views_data_alter(array &$data) {

  $data['node']['recordings'] = [
    'title' => t('Recordings for Artist'),
    'field' => [
      'title' => t('Recordings'),
      'help' => t('Recordings for a particular artist.'),
      'id' => 'rfn_views_recordings',
    ],
  ];

  $data['node']['tracks'] = [
    'title' => t('Tracks for Artist'),
    'field' => [
      'title' => t('Tracks'),
      'help' => t('Tracks for a particular artist.'),
      'id' => 'rfn_views_tracks',
    ],
  ];

}
